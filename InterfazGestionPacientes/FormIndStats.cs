﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms.DataVisualization.Charting;

namespace InterfazGestionPacientes {
    public partial class FormIndStats : Form {

        private SQLiteConnection _cx = new SQLiteConnection("Data Source=C:\\Database\\DBPacientes.db");
        private DataSet _data;
        private SQLiteDataAdapter _adapter;

        private int idPaciente;
        Patient patient = new Patient();

        private List<Registros> registros = new List<Registros>();

        public FormIndStats(int idPaciente) {
            InitializeComponent();
            this.idPaciente = idPaciente;
        }

        private void Form2_Load(object sender, EventArgs e) {
            getPatient();
            
            textBoxName.Text = patient.FullName;
            textBoxID.Text = patient.IdNumber;
            textBoxAge.Text = patient.Age.ToString();
            textBoxECVType.Text = patient.EcvType;
            textBoxECVTime.Text = patient.EcvTime.ToString();
            textBoxLat.Text = patient.Laterality;
            textBoxDom.Text = patient.Dominancy;
            textBoxDocType.Text = patient.DocType;
            textBoxAddress.Text = patient.Address;
            textBoxPhone.Text = patient.Telephone;
            textBoxOcupation.Text = patient.Ocupation;
            textBoxHealth.Text = patient.HealthLevel;

            comboBoxLevel.Items.Add("Easy");
            comboBoxLevel.Items.Add("Medium");
            comboBoxLevel.Items.Add("Hard");
            comboBoxLevel.Items.Add("VeryHard");
            comboBoxLevel.Items.Add("Legendary");
            comboBoxLevel.Items.Add("Personalized");
        }

        private void getPatient() {
            SQLiteCommand command = new SQLiteCommand("SELECT * FROM Pacientes WHERE Activo AND idPaciente = ?", _cx);
            command.Prepare();
            command.Parameters.AddWithValue("@idPaciente", idPaciente);
            command.CommandType = CommandType.Text;
            _cx.Open();

            var reader = command.ExecuteReader();

            try {
                while (reader.Read()) {

                    patient.FullName = (string)reader["NombreCompleto"];
                    patient.DocType = (string)reader["TipoDocumento"];
                    patient.IdNumber = (string)reader["NumDocumento"];
                    patient.Laterality = (string)reader["Lateridad"];
                    patient.Dominancy = (string)reader["Dominancia"];
                    patient.EcvTime = (int)reader["TiempoECV"];
                    patient.EcvType = (string)reader["TipoECV"];
                    patient.Telephone = (string)reader["Telefono"];
                    patient.Address = (string)reader["Direccion"];
                    patient.HealthLevel = (string)reader["RegimenSalud"];
                    patient.Age = (short)reader["Edad"];
                    patient.Ocupation = (string)reader["Ocupacion"];
                }
            } catch (Exception e) {
                Console.Write(e);
            }

            _cx.Close();
        }

        private void getRegisters() {
            string queryString = "SELECT AvgTime, TimeOfGame, LevelPlayed, ObjectsHit, ObjectsNotHit, GoodPoints, MediumPoints, BadPoints, Awards, Nivel.idSesion, Sesion.Fecha FROM Nivel JOIN Sesion ON Nivel.idSesion = Sesion.idSesion WHERE idPaciente = ? AND Nivel.LevelPlayed = ? ORDER BY Nivel.idSesion ASC";

            SQLiteCommand query = new SQLiteCommand(queryString, _cx);
            query.Prepare();
            query.Parameters.AddWithValue("@idPaciente", idPaciente);
            
            switch (comboBoxLevel.Text) {

                case "Easy":
                    query.Parameters.AddWithValue("@LevelPlayed", "easy");
                    break;

                case "Medium":
                    query.Parameters.AddWithValue("@LevelPlayed", "medium");
                    break;

                case "Hard":
                    query.Parameters.AddWithValue("@LevelPlayed", "hard");
                    break;

                case "VeryHard":
                    query.Parameters.AddWithValue("@LevelPlayed", "veryHard");
                    break;

                case "Legendary":
                    query.Parameters.AddWithValue("@LevelPlayed", "legend");
                    break;

                case "Personalized":
                    query.Parameters.AddWithValue("@LevelPlayed", "personalized");
                    break;

                default:
                    query.Parameters.AddWithValue("@LevelPlayed", "easy");
                    break;
            }
            query.CommandType = CommandType.Text;
            _cx.Open();

            var reader = query.ExecuteReader();

            try {

                while (reader.Read()) {
                    Registros registro = new Registros();
                    registro.IdSesion = reader.GetInt32(reader.GetOrdinal("idSesion"));
                    registro.AvgTime = reader.GetFloat(reader.GetOrdinal("AvgTime"));
                    registro.TimeOfGame = reader.GetFloat(reader.GetOrdinal("TimeOfGame"));
                    registro.LevelPlayed = reader.GetString(reader.GetOrdinal("LevelPlayed"));
                    registro.ObjectsHit = reader.GetInt32(reader.GetOrdinal("ObjectsHit"));
                    registro.ObjectsNotHit = reader.GetInt32(reader.GetOrdinal("ObjectsNotHit"));
                    registro.GoodPoints = reader.GetInt32(reader.GetOrdinal("GoodPoints"));
                    registro.MediumPoints = reader.GetInt32(reader.GetOrdinal("MediumPoints"));
                    registro.BadPoints = reader.GetInt32(reader.GetOrdinal("BadPoints"));
                    registro.Awards = reader.GetInt32(reader.GetOrdinal("Awards"));
                    registro.Date = reader.GetDateTime(reader.GetOrdinal("Fecha"));

                    registros.Add(registro);
                }
            } catch (Exception e) {
                Console.Write(e);
            }

            _cx.Close();
        }

        private void PlotData() {

            chartTime.Series.Add("Tiempo");
            chartTime.Titles.Add("Tiempo en Completar el Nivel");
            chartTime.Series["Tiempo"].ChartType = SeriesChartType.Line;
            chartTime.ChartAreas[0].AxisY.Title = "[Segundos]";
            chartTime.ChartAreas[0].AxisX.Title = "[Num Sesión]";

            DateTime Start = dateTimePicker1.Value.Date;
            DateTime Finish = dateTimePicker2.Value.Date;
            int count = 0;
            for (int i = 0; i < registros.Count; i++) {
                if (registros[i].Date.Date >= Start.Date && registros[i].Date.Date <= Finish.Date) {
                    chartTime.Series["Tiempo"].Points.AddXY(i , registros[i].AvgTime);
                    chartTime.Series["Tiempo"].Points[count].Label = "Sesión " + (count + 1);
                    count++;
                }

            }
        }

        private void buttonBack_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void label15_Click(object sender, EventArgs e) {

        }

        private void comboBoxLevel_SelectedIndexChanged(object sender, EventArgs e) {
            chartTime.Series.Clear();
            chartTime.Titles.Clear();
            registros = new List<Registros>();
            getRegisters();
            PlotData();
        }

        private void groupBox2_Enter(object sender, EventArgs e) {

        }
    }
}
