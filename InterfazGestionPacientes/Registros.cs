﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfazGestionPacientes {
    public class Registros {

        public Registros() {
        }

        public int IdSesion { get; set; }
        public float AvgTime { get; set; }
        public float TimeOfGame { get; set; }
        public string LevelPlayed { get; set; }
        public int ObjectsHit { get; set; }
        public int ObjectsNotHit { get; set; }
        public int GoodPoints { get; set; }
        public int MediumPoints { get; set; }
        public int BadPoints { get; set; }
        public int Awards { get; set; }
        public DateTime Date { get; set; }
    }
}
