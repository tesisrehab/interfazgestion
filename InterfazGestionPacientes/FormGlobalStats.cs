﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Xml.Linq;

namespace InterfazGestionPacientes
{
    public partial class FormGlobalStats : Form
    {
        private SQLiteConnection _cx = new SQLiteConnection("Data Source=C:\\Database\\DBPacientes.db");
        private DataSet _data;
        private SQLiteDataAdapter _adapter;

        private int[] ageDist = new int[4] { 0, 0, 0, 0 };
        int [] gender = new int[] {0, 0};
        int[] latDist = new int[3] { 0, 0, 0 };
        int[] ecvType = new int[] {0, 0, 0, 0};

        List<Patient> Patients = new List<Patient>(); 

        public FormGlobalStats()
        {
            InitializeComponent();
        }

        private void FormGlobalStats_Load ( object sender, EventArgs e ) {
            getPatients();
            PlotData();
        }

        private void getPatients() {
            SQLiteCommand command = new SQLiteCommand("SELECT * FROM Pacientes WHERE Activo", _cx);
            command.CommandType = CommandType.Text;
            _cx.Open();
            var reader = command.ExecuteReader();

            try {
                while (reader.Read()) {
                    Patient temp = new Patient();
                    temp.Age = (short) reader["Edad"];
                    temp.Laterality = (string) reader["Lateridad"];
                    temp.EcvType = (string) reader["TipoECV"];
                    temp.Sex = (string) reader["Sexo"];
                    Patients.Add(temp);
                }
            }
            catch (Exception e) {
                Console.Write(e);
            }
            ArrangeData();
            _cx.Close();
        }

        private void ArrangeData() {
            for (int i = 0; i < Patients.Count; i++) {

                if (Patients[i].Age > 0 && Patients[i].Age <= 20) ageDist[0] += 1;
                if (Patients[i].Age > 20 && Patients[i].Age <= 40) ageDist[1] += 1;
                if (Patients[i].Age > 40 && Patients[i].Age <= 60) ageDist[2] += 1;
                if (Patients[i].Age > 60 && Patients[i].Age <= 80) ageDist[3] += 1;

                if (Patients[i].Laterality.Equals("Derecha")) latDist[0] += 1;
                if(Patients[i].Laterality.Equals("Izquierda")) latDist[1] += 1;
                if (Patients[i].Laterality.Equals("Bilateral")) latDist[2] += 1;

                if (Patients[i].Sex.Equals("Masculino")) gender[0] += 1;
                if (Patients[i].Sex.Equals("Femenino")) gender[1] += 1;

                if (Patients[i].EcvType.Equals("Trombótico")) ecvType[0] += 1;
                if (Patients[i].EcvType.Equals("Embólico")) ecvType[1] += 1;
                if (Patients[i].EcvType.Equals("Lacunar")) ecvType[2] += 1;
                if (Patients[i].EcvType.Equals("Hemorrágico")) ecvType[3] += 1;

            }
        }

        private void PlotData() {
            
            chartAge.Series.Add("Edades");
            chartAge.Titles.Add("Clasificación de Edades");
            chartAge.Palette = ChartColorPalette.BrightPastel;
            
            chartAge.Series["Edades"].Points.AddXY("0-20",  ageDist[0]);
            chartAge.Series["Edades"].Points.AddXY("21-40", ageDist[1]);
            chartAge.Series["Edades"].Points.AddXY("41-60", ageDist[2]);
            chartAge.Series["Edades"].Points.AddXY("61-80", ageDist[3]);

            chartLat.Series.Add("Lateridad");
            chartLat.Titles.Add("Lateridad");
            chartLat.Series["Lateridad"].ChartType = SeriesChartType.Bar;
            chartLat.Palette = ChartColorPalette.Pastel;
 
            chartLat.Series["Lateridad"].Points.AddXY("Der", latDist[0]);
            chartLat.Series["Lateridad"].Points.AddXY("Izq", latDist[1]);
            chartLat.Series["Lateridad"].Points.AddXY("Bilateral", latDist[2]);

            chartSex.Series.Add("Sexo");
            chartSex.Titles.Add("Sexo");
            chartSex.Series["Sexo"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Doughnut;
            chartSex.Palette = ChartColorPalette.SeaGreen;

            float totalPat = gender[0] + gender[1];
            float percMan = (gender[0]/totalPat) * 100;
            float percFem = (gender[1] / totalPat) * 100;

            chartSex.Series["Sexo"].Points.AddXY(1, gender[0]);
            chartSex.Series["Sexo"].Points[0].Label = "Hombres - " + Math.Round(percMan, 2) + "%";
            chartSex.Series["Sexo"].Points.AddXY(2, gender[1]);
            chartSex.Series["Sexo"].Points[1].Label = "Mujeres - " + Math.Round(percFem, 2) + "%";

            chartECV.Series.Add("Tipo ECV");
            chartECV.Titles.Add("Clasificación");
            chartECV.Series["Tipo ECV"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            chartECV.Palette = ChartColorPalette.None;

            float totalReg = ecvType[0] + ecvType[1] + ecvType[2] + ecvType[3];
            float trom = (ecvType[0]/totalReg)*100;
            float emb = (ecvType[1]/totalReg)*100;
            float lac = (ecvType[2]/totalReg)*100;
            float hem = (ecvType[3]/totalReg)*100;

            chartECV.Series["Tipo ECV"].Points.AddXY(1, ecvType[0]);
            chartECV.Series["Tipo ECV"].Points[0].Label = Math.Round(trom, 2) + "%";
            chartECV.Series["Tipo ECV"].Points[0].LegendText = "Trombótico";
            chartECV.Series["Tipo ECV"].Points.AddXY(2, ecvType[1]);
            chartECV.Series["Tipo ECV"].Points[1].Label = Math.Round(emb, 2) + "%";
            chartECV.Series["Tipo ECV"].Points[1].LegendText = "Embólico";
            chartECV.Series["Tipo ECV"].Points.AddXY(3, ecvType[2]);
            chartECV.Series["Tipo ECV"].Points[2].Label = Math.Round(lac, 2) + "%";
            chartECV.Series["Tipo ECV"].Points[2].LegendText = "Lacunar";
            chartECV.Series["Tipo ECV"].Points.AddXY(4, ecvType[3]);
            chartECV.Series["Tipo ECV"].Points[3].Label = Math.Round(hem, 2) + "%";
            chartECV.Series["Tipo ECV"].Points[3].LegendText = "Hemorrágico";

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
