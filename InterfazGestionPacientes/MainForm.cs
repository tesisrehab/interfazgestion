﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Diagnostics;

namespace InterfazGestionPacientes
{
    public partial class MainForm : Form
    {
        private SQLiteConnection _cx = new SQLiteConnection("Data Source=C:\\Database\\DBPacientes.db");
        private DataSet _data;
        private SQLiteDataAdapter _adapter;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
             GetRecords();
        }

        private void buttonAddPatient_Click(object sender, EventArgs e)
        {
            FormAddPatient formAdd = new FormAddPatient();
            formAdd.Show();
        }

        public void GetRecords()
        {
            SQLiteCommand command = new SQLiteCommand("SELECT * FROM Pacientes WHERE Activo", _cx);
            command.CommandType = CommandType.Text;
            _data = new DataSet();
            _adapter = new SQLiteDataAdapter(command);
            _adapter.Fill(_data, "Pacientes");
            dataGridViewPatients.DataSource = _data.Tables["Pacientes"];
        }


        private void buttonUpdatePatient_Click ( object sender, EventArgs e ) {
            try {
                SQLiteCommandBuilder comm = new SQLiteCommandBuilder(_adapter);
                _adapter.Update(_data, "Pacientes");
                MessageBox.Show("Información Actualizada", "Actualizar", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void textBoxFilterValue_TextChanged ( object sender, EventArgs e ) {
            if (comboBoxFilter.Text == "Documento de Identidad") {
                SQLiteCommand command = new SQLiteCommand("SELECT * FROM Pacientes WHERE NumDocumento LIKE '"+ textBoxFilterValue.Text + "%' AND Activo", _cx);
                command.CommandType = CommandType.Text;
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(command);
                DataTable pacData = new DataTable();
                adapter.Fill(pacData);
                dataGridViewPatients.DataSource = pacData;
                Refresh();
            }

            if (comboBoxFilter.Text == "Nombre") {
                SQLiteCommand command = new SQLiteCommand("SELECT * FROM Pacientes WHERE NombreCompleto LIKE '" + textBoxFilterValue.Text + "%' AND Activo", _cx);
                command.CommandType = CommandType.Text;
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(command);
                DataTable pacData = new DataTable();
                adapter.Fill(pacData);
                dataGridViewPatients.DataSource = pacData;
                Refresh();
            }

            if (comboBoxFilter.Text == "" || comboBoxFilter.Text == null) {
                Console.Write("Pass");
            }

        }

        private void buttonDisablePatient_Click ( object sender, EventArgs e ) {
            if (dataGridViewPatients.SelectedRows.Count > 0) {
                int selectedRowIndex = dataGridViewPatients.SelectedCells[0].RowIndex;
                DataGridViewRow selectedRow = dataGridViewPatients.Rows[selectedRowIndex];
                var id = dataGridViewPatients.SelectedRows[0].Cells[0].Value; 

                SQLiteCommand disable = new SQLiteCommand("UPDATE Pacientes SET Activo = 0 WHERE idPaciente = ?",_cx); 
                disable.Prepare();
                disable.Parameters.AddWithValue("@idPaciente", id);

                try {
                    _cx.Open();
                    disable.ExecuteNonQuery();
                    _cx.Close();
                    MessageBox.Show("Se desactivo el registro con éxito");                
                    GetRecords();
                }
                catch (Exception ex) {
                    MessageBox.Show("Error al agregar registro a la base de datos");
                    throw new Exception(ex.Message);
                }

            }
        }

        private void buttonStats_Click ( object sender, EventArgs e ) {
            if (dataGridViewPatients.SelectedRows.Count == 1) {
                int selectedRowIndex = dataGridViewPatients.SelectedCells[0].RowIndex;
                DataGridViewRow selectedRow = dataGridViewPatients.Rows[selectedRowIndex];
                var id = dataGridViewPatients.SelectedRows[0].Cells[0].Value;
                int idPac = int.Parse(id.ToString());
                FormIndStats indStats = new FormIndStats(idPac);
                indStats.Show();
            }
            else {
                FormGlobalStats formGlobal = new FormGlobalStats();
                formGlobal.Show();
            }
                
        }

        private void buttonSession_Click ( object sender, EventArgs e ) {
            if (dataGridViewPatients.SelectedRows.Count == 1) {
                int selectedRowIndex = dataGridViewPatients.SelectedCells[0].RowIndex;
                DataGridViewRow selectedRow = dataGridViewPatients.Rows[selectedRowIndex];
                var id = dataGridViewPatients.SelectedRows[0].Cells[0].Value;

                SQLiteCommand setSession = new SQLiteCommand("UPDATE sesion_actual SET idPaciente = ?", _cx);
                setSession.Prepare();
                setSession.Parameters.AddWithValue("@idPaciente", id);

                try {
                    _cx.Open();
                    setSession.ExecuteNonQuery();
                    _cx.Close();
                    MessageBox.Show("Se estableció la sesión correctamente");
                    GetRecords();
                }
                catch (Exception ex) {
                    MessageBox.Show("Error al agregar registro a la base de datos");
                    throw new Exception(ex.Message);
                }

            }
        }

        private void dataGridViewPatients_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //GetRecords();
        }
    }
}
