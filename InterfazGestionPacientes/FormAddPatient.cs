﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;


namespace InterfazGestionPacientes
{
    public partial class FormAddPatient : Form
    {

        private Patient patient;
        private MainForm main;

        SQLiteConnection cx = new SQLiteConnection("Data Source=C:\\Database\\DBPacientes.db");

        public FormAddPatient()
        {
            InitializeComponent();
        }

        public FormAddPatient(Form callForm)
        {
            main = callForm as MainForm;
            InitializeComponent();
        }

        private void FormAddPatient_Load(object sender, EventArgs e)
        {
            //Instanciación del objeto Paciente
            patient = new Patient();

            //Elementos para tipo de Documento
            comboBoxDocType.Items.Add("Registro civil ");
            comboBoxDocType.Items.Add("Tarjeta de identidad");
            comboBoxDocType.Items.Add("Cédula");
            comboBoxDocType.Items.Add("Pasaporte");
            comboBoxDocType.Items.Add("Cédula de extranjeria");

            //Elementos Sexo
            comboBoxSex.Items.Add("Masculino");
            comboBoxSex.Items.Add("Femenino");

            //Elementos Estado Civil
            comboBoxCivState.Items.Add("Soltero");
            comboBoxCivState.Items.Add("Casado");
            comboBoxCivState.Items.Add("Viudo");
            comboBoxCivState.Items.Add("Separado");
            comboBoxCivState.Items.Add("Unión libre");

            //Elementos Escolaridad
            comboBoxSchool.Items.Add("Primaria");
            comboBoxSchool.Items.Add("Secundaria");
            comboBoxSchool.Items.Add("Técnico");
            comboBoxSchool.Items.Add("Tecnólogo");
            comboBoxSchool.Items.Add("Profesional");
            comboBoxSchool.Items.Add("Posgrado");
            comboBoxSchool.Items.Add("Ninguno");

            //Elementos Regimen de Salud
            comboBoxHealth.Items.Add("Vinculado");
            comboBoxHealth.Items.Add("Subsidiado");
            comboBoxHealth.Items.Add("Contributivo");
            comboBoxHealth.Items.Add("Especial");
            comboBoxHealth.Items.Add("SOAT");
            comboBoxHealth.Items.Add("Ninguno");

            //Elementos Ocupación
            comboBoxOcupation.Items.Add("Empleado");
            comboBoxOcupation.Items.Add("Independiente");
            comboBoxOcupation.Items.Add("Ama de Casa");
            comboBoxOcupation.Items.Add("Pensionado");
            comboBoxOcupation.Items.Add("Estudiante");
            comboBoxOcupation.Items.Add("Desempleado");
            comboBoxOcupation.Items.Add("Otro");

            //Elementos Tipo de ECV
            comboBoxECVType.Items.Add("Trombótico");
            comboBoxECVType.Items.Add("Embólico");
            comboBoxECVType.Items.Add("Lacunar");
            comboBoxECVType.Items.Add("Hemorrágico");

            //Elementos Lateridad
            comboBoxLat.Items.Add("Derecha");
            comboBoxLat.Items.Add("Izquierda");
            comboBoxLat.Items.Add("Bilateral");

            //Elementos Dominancia
            comboBoxDom.Items.Add("Diestro");
            comboBoxDom.Items.Add("Siniestro");
            comboBoxDom.Items.Add("Ambidiestro");
        }

        private Patient createPatient()
        {
            Patient paciente = new Patient();
            try
            {
                if (textBoxFullName.Text != "" && comboBoxDocType.Text != "" && textBoxID.Text != "" &&
                    textBoxAge.Text != "" && comboBoxSex.Text != "")
                {
                    paciente.FullName = textBoxFullName.Text;
                    paciente.DocType = comboBoxDocType.Text;
                    paciente.IdNumber = textBoxID.Text;
                    paciente.RegDate = DateTime.Now;
                    if(textBoxAge.Text != "") paciente.Age = Convert.ToInt16(textBoxAge.Text);
                    paciente.Sex = comboBoxSex.Text;
                    paciente.CivState = comboBoxCivState.Text;
                    paciente.Scolarship = comboBoxSchool.Text;
                    paciente.HealthLevel = comboBoxHealth.Text;
                    paciente.Ocupation = comboBoxOcupation.Text;
                    paciente.BirthPlace = textBoxBirthPlace.Text;
                    paciente.Residency = textBoxResidency.Text;
                    paciente.Address = textBoxAddress.Text;
                    paciente.Telephone = textBoxPhone.Text;
                    paciente.EcvType = comboBoxECVType.Text;
                    if (textBoxECVTime.Text != "") paciente.EcvTime = Convert.ToInt16(textBoxECVTime.Text);
                    paciente.Laterality = comboBoxLat.Text;
                    paciente.Dominancy = comboBoxDom.Text;
                }
                else
                {
                    paciente = null;
                }
                

            }
            catch (Exception err)
            {
                paciente = null;
            }

            return paciente;

        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            patient = createPatient();
            

            if (patient != null)
            {
                cx.Open();
                SQLiteCommand insertPatient = new SQLiteCommand("INSERT INTO Pacientes (NombreCompleto, TipoDocumento, NumDocumento, FechaReg, " +
                                                                "Edad, Sexo, EstadoCivil, Escolaridad, RegimenSalud, Ocupacion, LugarNacimiento, " +
                                                                "LugarResidencia, Direccion, Telefono, TipoECV, TiempoECV, Lateridad, Dominancia, Activo) " +
                                                                " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", cx);
                insertPatient.Prepare();

                insertPatient.Parameters.AddWithValue("@NombreCompleto", patient.FullName);
                insertPatient.Parameters.AddWithValue("@TipoDocumento", patient.DocType);
                insertPatient.Parameters.AddWithValue("@NumDocumento", patient.IdNumber);
                insertPatient.Parameters.AddWithValue("@FechaReg", patient.RegDate);
                insertPatient.Parameters.AddWithValue("@Edad", patient.Age);
                insertPatient.Parameters.AddWithValue("@Sexo", patient.Sex);
                insertPatient.Parameters.AddWithValue("@EstadoCivil", patient.CivState);
                insertPatient.Parameters.AddWithValue("@Escolaridad", patient.Scolarship);
                insertPatient.Parameters.AddWithValue("@RegimenSalud", patient.HealthLevel);
                insertPatient.Parameters.AddWithValue("@Ocupacion", patient.Ocupation);
                insertPatient.Parameters.AddWithValue("@LugarNacimiento", patient.BirthPlace);
                insertPatient.Parameters.AddWithValue("@LugarResidencia", patient.Residency);
                insertPatient.Parameters.AddWithValue("@Direccion", patient.Address);
                insertPatient.Parameters.AddWithValue("@Telefono", patient.Telephone);
                insertPatient.Parameters.AddWithValue("@TipoECV", patient.EcvType);
                insertPatient.Parameters.AddWithValue("@TiempoECV", patient.EcvTime);
                insertPatient.Parameters.AddWithValue("@Lateridad", patient.Laterality);
                insertPatient.Parameters.AddWithValue("@Dominancia", patient.Dominancy);
                insertPatient.Parameters.AddWithValue("@Activo", true);

                try
                {
                    insertPatient.ExecuteNonQuery();
                    cx.Close();

                    if (System.Windows.Forms.Application.OpenForms["MainForm"] != null) {
                        (System.Windows.Forms.Application.OpenForms["MainForm"] as MainForm).GetRecords();
                    }
                    this.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error al agregar registro a la base de datos");
                    throw new Exception(ex.Message);             
                }
            }
            else
            {
                MessageBox.Show("Por favor ingrese los campos obligatorios");
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
