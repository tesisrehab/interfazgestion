﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfazGestionPacientes
{
    public class Patient
    {
        public Patient()
        {
            FullName = "";
            DocType = "";
            IdNumber = "";
            Age = 0;
            Sex = "";
            CivState = "";
            Scolarship = "";
            HealthLevel = "";
            Ocupation = "";
            BirthPlace = "";
            Residency = "";
            Address = "";
            Telephone = "";
            EcvType = "";
            EcvTime = 0;
            Laterality = "";
            Dominancy = "";
        }


        public string FullName { get; set; }

        public string DocType { get; set; }

        public string IdNumber { get; set; }

        public  DateTime RegDate { get; set; }

        public short Age { get; set; }

        public string Sex { get; set; }

        public string CivState { get; set; }

        public string Scolarship { get; set; }

        public string HealthLevel { get; set; }

        public string Ocupation { get; set; }

        public string BirthPlace { get; set; }

        public string Residency { get; set; }

        public string Address { get; set; }

        public string Telephone { get; set; }

        public string EcvType { get; set; }

        public int EcvTime { get; set; }

        public string Laterality { get; set; }

        public string Dominancy { get; set; }
    }
}
