﻿namespace InterfazGestionPacientes
{
    partial class FormAddPatient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.textBoxFullName = new System.Windows.Forms.TextBox();
            this.comboBoxDocType = new System.Windows.Forms.ComboBox();
            this.textBoxID = new System.Windows.Forms.TextBox();
            this.textBoxAge = new System.Windows.Forms.TextBox();
            this.comboBoxSex = new System.Windows.Forms.ComboBox();
            this.comboBoxCivState = new System.Windows.Forms.ComboBox();
            this.comboBoxSchool = new System.Windows.Forms.ComboBox();
            this.comboBoxHealth = new System.Windows.Forms.ComboBox();
            this.comboBoxOcupation = new System.Windows.Forms.ComboBox();
            this.textBoxBirthPlace = new System.Windows.Forms.TextBox();
            this.textBoxResidency = new System.Windows.Forms.TextBox();
            this.textBoxAddress = new System.Windows.Forms.TextBox();
            this.textBoxPhone = new System.Windows.Forms.TextBox();
            this.comboBoxECVType = new System.Windows.Forms.ComboBox();
            this.comboBoxLat = new System.Windows.Forms.ComboBox();
            this.comboBoxDom = new System.Windows.Forms.ComboBox();
            this.textBoxECVTime = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(43, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre completo";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(43, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Tipo de documento";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(43, 130);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Número de documento";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(43, 175);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Edad ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(43, 220);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Sexo";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(43, 265);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Estado civil";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(43, 310);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(103, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Nivel de escolaridad";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(43, 355);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(92, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Régimen de salud";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(43, 400);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Ocupación";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(446, 40);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(103, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "Lugar de nacimiento";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(446, 85);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(100, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Lugar de residencia";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(446, 130);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "Dirección";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(446, 175);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(49, 13);
            this.label13.TabIndex = 12;
            this.label13.Text = "Teléfono";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(446, 220);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(67, 13);
            this.label14.TabIndex = 13;
            this.label14.Text = "Tipo de ECV";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(446, 265);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(149, 13);
            this.label15.TabIndex = 14;
            this.label15.Text = "Tiempo desde el ECV (Meses)";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(446, 310);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(59, 13);
            this.label16.TabIndex = 15;
            this.label16.Text = "Lateralidad";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(446, 355);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(63, 13);
            this.label17.TabIndex = 16;
            this.label17.Text = "Dominancia";
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(223, 450);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 23);
            this.buttonSave.TabIndex = 17;
            this.buttonSave.Text = "Guardar";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(449, 450);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 18;
            this.buttonCancel.Text = "Cancelar";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // textBoxFullName
            // 
            this.textBoxFullName.Location = new System.Drawing.Point(174, 40);
            this.textBoxFullName.Name = "textBoxFullName";
            this.textBoxFullName.Size = new System.Drawing.Size(123, 20);
            this.textBoxFullName.TabIndex = 19;
            // 
            // comboBoxDocType
            // 
            this.comboBoxDocType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDocType.FormattingEnabled = true;
            this.comboBoxDocType.Location = new System.Drawing.Point(174, 83);
            this.comboBoxDocType.Name = "comboBoxDocType";
            this.comboBoxDocType.Size = new System.Drawing.Size(123, 21);
            this.comboBoxDocType.TabIndex = 20;
            // 
            // textBoxID
            // 
            this.textBoxID.Location = new System.Drawing.Point(174, 127);
            this.textBoxID.Name = "textBoxID";
            this.textBoxID.Size = new System.Drawing.Size(123, 20);
            this.textBoxID.TabIndex = 21;
            // 
            // textBoxAge
            // 
            this.textBoxAge.Location = new System.Drawing.Point(175, 170);
            this.textBoxAge.Name = "textBoxAge";
            this.textBoxAge.Size = new System.Drawing.Size(123, 20);
            this.textBoxAge.TabIndex = 22;
            // 
            // comboBoxSex
            // 
            this.comboBoxSex.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSex.FormattingEnabled = true;
            this.comboBoxSex.Location = new System.Drawing.Point(174, 213);
            this.comboBoxSex.Name = "comboBoxSex";
            this.comboBoxSex.Size = new System.Drawing.Size(123, 21);
            this.comboBoxSex.TabIndex = 23;
            // 
            // comboBoxCivState
            // 
            this.comboBoxCivState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCivState.FormattingEnabled = true;
            this.comboBoxCivState.Location = new System.Drawing.Point(174, 257);
            this.comboBoxCivState.Name = "comboBoxCivState";
            this.comboBoxCivState.Size = new System.Drawing.Size(123, 21);
            this.comboBoxCivState.TabIndex = 24;
            // 
            // comboBoxSchool
            // 
            this.comboBoxSchool.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSchool.FormattingEnabled = true;
            this.comboBoxSchool.Location = new System.Drawing.Point(174, 301);
            this.comboBoxSchool.Name = "comboBoxSchool";
            this.comboBoxSchool.Size = new System.Drawing.Size(123, 21);
            this.comboBoxSchool.TabIndex = 25;
            // 
            // comboBoxHealth
            // 
            this.comboBoxHealth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxHealth.FormattingEnabled = true;
            this.comboBoxHealth.Location = new System.Drawing.Point(174, 345);
            this.comboBoxHealth.Name = "comboBoxHealth";
            this.comboBoxHealth.Size = new System.Drawing.Size(123, 21);
            this.comboBoxHealth.TabIndex = 26;
            // 
            // comboBoxOcupation
            // 
            this.comboBoxOcupation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxOcupation.FormattingEnabled = true;
            this.comboBoxOcupation.Location = new System.Drawing.Point(174, 389);
            this.comboBoxOcupation.Name = "comboBoxOcupation";
            this.comboBoxOcupation.Size = new System.Drawing.Size(123, 21);
            this.comboBoxOcupation.TabIndex = 27;
            // 
            // textBoxBirthPlace
            // 
            this.textBoxBirthPlace.Location = new System.Drawing.Point(596, 40);
            this.textBoxBirthPlace.Name = "textBoxBirthPlace";
            this.textBoxBirthPlace.Size = new System.Drawing.Size(119, 20);
            this.textBoxBirthPlace.TabIndex = 28;
            // 
            // textBoxResidency
            // 
            this.textBoxResidency.Location = new System.Drawing.Point(596, 84);
            this.textBoxResidency.Name = "textBoxResidency";
            this.textBoxResidency.Size = new System.Drawing.Size(119, 20);
            this.textBoxResidency.TabIndex = 29;
            // 
            // textBoxAddress
            // 
            this.textBoxAddress.Location = new System.Drawing.Point(596, 128);
            this.textBoxAddress.Name = "textBoxAddress";
            this.textBoxAddress.Size = new System.Drawing.Size(119, 20);
            this.textBoxAddress.TabIndex = 30;
            // 
            // textBoxPhone
            // 
            this.textBoxPhone.Location = new System.Drawing.Point(596, 172);
            this.textBoxPhone.Name = "textBoxPhone";
            this.textBoxPhone.Size = new System.Drawing.Size(119, 20);
            this.textBoxPhone.TabIndex = 31;
            // 
            // comboBoxECVType
            // 
            this.comboBoxECVType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxECVType.FormattingEnabled = true;
            this.comboBoxECVType.Location = new System.Drawing.Point(596, 216);
            this.comboBoxECVType.Name = "comboBoxECVType";
            this.comboBoxECVType.Size = new System.Drawing.Size(119, 21);
            this.comboBoxECVType.TabIndex = 32;
            // 
            // comboBoxLat
            // 
            this.comboBoxLat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxLat.FormattingEnabled = true;
            this.comboBoxLat.Location = new System.Drawing.Point(596, 305);
            this.comboBoxLat.Name = "comboBoxLat";
            this.comboBoxLat.Size = new System.Drawing.Size(119, 21);
            this.comboBoxLat.TabIndex = 33;
            // 
            // comboBoxDom
            // 
            this.comboBoxDom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDom.FormattingEnabled = true;
            this.comboBoxDom.Location = new System.Drawing.Point(596, 350);
            this.comboBoxDom.Name = "comboBoxDom";
            this.comboBoxDom.Size = new System.Drawing.Size(119, 21);
            this.comboBoxDom.TabIndex = 34;
            // 
            // textBoxECVTime
            // 
            this.textBoxECVTime.Location = new System.Drawing.Point(596, 261);
            this.textBoxECVTime.Name = "textBoxECVTime";
            this.textBoxECVTime.Size = new System.Drawing.Size(119, 20);
            this.textBoxECVTime.TabIndex = 35;
            // 
            // FormAddPatient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(751, 509);
            this.Controls.Add(this.textBoxECVTime);
            this.Controls.Add(this.comboBoxDom);
            this.Controls.Add(this.comboBoxLat);
            this.Controls.Add(this.comboBoxECVType);
            this.Controls.Add(this.textBoxPhone);
            this.Controls.Add(this.textBoxAddress);
            this.Controls.Add(this.textBoxResidency);
            this.Controls.Add(this.textBoxBirthPlace);
            this.Controls.Add(this.comboBoxOcupation);
            this.Controls.Add(this.comboBoxHealth);
            this.Controls.Add(this.comboBoxSchool);
            this.Controls.Add(this.comboBoxCivState);
            this.Controls.Add(this.comboBoxSex);
            this.Controls.Add(this.textBoxAge);
            this.Controls.Add(this.textBoxID);
            this.Controls.Add(this.comboBoxDocType);
            this.Controls.Add(this.textBoxFullName);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FormAddPatient";
            this.Text = "FormAddPatient";
            this.Load += new System.EventHandler(this.FormAddPatient_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.TextBox textBoxFullName;
        private System.Windows.Forms.ComboBox comboBoxDocType;
        private System.Windows.Forms.TextBox textBoxID;
        private System.Windows.Forms.TextBox textBoxAge;
        private System.Windows.Forms.ComboBox comboBoxSex;
        private System.Windows.Forms.ComboBox comboBoxCivState;
        private System.Windows.Forms.ComboBox comboBoxSchool;
        private System.Windows.Forms.ComboBox comboBoxHealth;
        private System.Windows.Forms.ComboBox comboBoxOcupation;
        private System.Windows.Forms.TextBox textBoxBirthPlace;
        private System.Windows.Forms.TextBox textBoxResidency;
        private System.Windows.Forms.TextBox textBoxAddress;
        private System.Windows.Forms.TextBox textBoxPhone;
        private System.Windows.Forms.ComboBox comboBoxECVType;
        private System.Windows.Forms.ComboBox comboBoxLat;
        private System.Windows.Forms.ComboBox comboBoxDom;
        private System.Windows.Forms.TextBox textBoxECVTime;
    }
}