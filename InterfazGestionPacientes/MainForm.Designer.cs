﻿namespace InterfazGestionPacientes
{
    partial class MainForm
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.buttonAddPatient = new System.Windows.Forms.Button();
            this.buttonUpdatePatient = new System.Windows.Forms.Button();
            this.dataGridViewPatients = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.buttonDisablePatient = new System.Windows.Forms.Button();
            this.comboBoxFilter = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxFilterValue = new System.Windows.Forms.TextBox();
            this.buttonStats = new System.Windows.Forms.Button();
            this.buttonSession = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPatients)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonAddPatient
            // 
            this.buttonAddPatient.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.buttonAddPatient.Location = new System.Drawing.Point(34, 195);
            this.buttonAddPatient.Name = "buttonAddPatient";
            this.buttonAddPatient.Size = new System.Drawing.Size(137, 34);
            this.buttonAddPatient.TabIndex = 0;
            this.buttonAddPatient.Text = "Agregar Paciente";
            this.buttonAddPatient.UseVisualStyleBackColor = true;
            this.buttonAddPatient.Click += new System.EventHandler(this.buttonAddPatient_Click);
            // 
            // buttonUpdatePatient
            // 
            this.buttonUpdatePatient.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.buttonUpdatePatient.Location = new System.Drawing.Point(34, 241);
            this.buttonUpdatePatient.Name = "buttonUpdatePatient";
            this.buttonUpdatePatient.Size = new System.Drawing.Size(137, 34);
            this.buttonUpdatePatient.TabIndex = 1;
            this.buttonUpdatePatient.Text = "Actualizar Datos";
            this.buttonUpdatePatient.UseVisualStyleBackColor = true;
            this.buttonUpdatePatient.Click += new System.EventHandler(this.buttonUpdatePatient_Click);
            // 
            // dataGridViewPatients
            // 
            this.dataGridViewPatients.AllowUserToOrderColumns = true;
            this.dataGridViewPatients.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewPatients.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPatients.Location = new System.Drawing.Point(288, 195);
            this.dataGridViewPatients.Name = "dataGridViewPatients";
            this.dataGridViewPatients.Size = new System.Drawing.Size(516, 172);
            this.dataGridViewPatients.TabIndex = 4;
            this.dataGridViewPatients.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewPatients_CellContentClick);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(108, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(437, 33);
            this.label1.TabIndex = 5;
            this.label1.Text = "Interfáz de Gestión de Pacientes";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(765, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(107, 132);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // buttonDisablePatient
            // 
            this.buttonDisablePatient.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.buttonDisablePatient.Location = new System.Drawing.Point(34, 287);
            this.buttonDisablePatient.Name = "buttonDisablePatient";
            this.buttonDisablePatient.Size = new System.Drawing.Size(137, 34);
            this.buttonDisablePatient.TabIndex = 3;
            this.buttonDisablePatient.Text = "Desactivar Registro";
            this.buttonDisablePatient.UseVisualStyleBackColor = true;
            this.buttonDisablePatient.Click += new System.EventHandler(this.buttonDisablePatient_Click);
            // 
            // comboBoxFilter
            // 
            this.comboBoxFilter.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comboBoxFilter.Cursor = System.Windows.Forms.Cursors.Default;
            this.comboBoxFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxFilter.FormattingEnabled = true;
            this.comboBoxFilter.Items.AddRange(new object[] {
            "Documento de Identidad",
            "Nombre"});
            this.comboBoxFilter.Location = new System.Drawing.Point(369, 169);
            this.comboBoxFilter.Name = "comboBoxFilter";
            this.comboBoxFilter.Size = new System.Drawing.Size(146, 21);
            this.comboBoxFilter.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(289, 172);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Seleccione";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxFilterValue
            // 
            this.textBoxFilterValue.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBoxFilterValue.Location = new System.Drawing.Point(551, 170);
            this.textBoxFilterValue.Name = "textBoxFilterValue";
            this.textBoxFilterValue.Size = new System.Drawing.Size(129, 20);
            this.textBoxFilterValue.TabIndex = 10;
            this.textBoxFilterValue.TextChanged += new System.EventHandler(this.textBoxFilterValue_TextChanged);
            // 
            // buttonStats
            // 
            this.buttonStats.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.buttonStats.Location = new System.Drawing.Point(34, 333);
            this.buttonStats.Name = "buttonStats";
            this.buttonStats.Size = new System.Drawing.Size(137, 34);
            this.buttonStats.TabIndex = 11;
            this.buttonStats.Text = "Generar Reportes";
            this.buttonStats.UseVisualStyleBackColor = true;
            this.buttonStats.Click += new System.EventHandler(this.buttonStats_Click);
            // 
            // buttonSession
            // 
            this.buttonSession.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.buttonSession.Location = new System.Drawing.Point(34, 382);
            this.buttonSession.Name = "buttonSession";
            this.buttonSession.Size = new System.Drawing.Size(137, 34);
            this.buttonSession.TabIndex = 12;
            this.buttonSession.Text = "Establecer Sesión";
            this.buttonSession.UseVisualStyleBackColor = true;
            this.buttonSession.Click += new System.EventHandler(this.buttonSession_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 461);
            this.Controls.Add(this.buttonSession);
            this.Controls.Add(this.buttonStats);
            this.Controls.Add(this.textBoxFilterValue);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBoxFilter);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridViewPatients);
            this.Controls.Add(this.buttonDisablePatient);
            this.Controls.Add(this.buttonUpdatePatient);
            this.Controls.Add(this.buttonAddPatient);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "Interfáz de Gestión de Pacientes";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPatients)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonAddPatient;
        private System.Windows.Forms.Button buttonUpdatePatient;
        private System.Windows.Forms.DataGridView dataGridViewPatients;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button buttonDisablePatient;
        private System.Windows.Forms.ComboBox comboBoxFilter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxFilterValue;
        private System.Windows.Forms.Button buttonStats;
        private System.Windows.Forms.Button buttonSession;
    }
}

